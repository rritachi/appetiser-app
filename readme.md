# README #

### Persistence use ###
* Use the simple App Preference for persisting to save the last item viewed and and date time access. App Preference for me is the best way to save simple data as for this example because its straight forward and memory sufficient.

### Design pattern  Use ###
* Creational Design pattern
	* using Singleton for calling function without initiation 
	* Build the project as what the design or features outcome would be

### 3rd party library use  ###
* [Picasso](https://square.github.io/picasso/) for external image and caching