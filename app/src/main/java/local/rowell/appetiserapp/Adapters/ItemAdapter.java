package local.rowell.appetiserapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import local.rowell.appetiserapp.Models.Item;
import local.rowell.appetiserapp.R;

public class ItemAdapter extends ArrayAdapter<Item> {

    private final Context context;
    private final ArrayList<Item> items;
    private int resource;

    public ItemAdapter(Context context, int resource, ArrayList<Item> items) {
        super(context, resource, items);

        this.context = context;
        this.items = items;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource, parent, false);

            viewHolder.imageViewCover = convertView.findViewById(R.id.cover_image);
            viewHolder.textViewGenre = convertView.findViewById(R.id.text_view_genre);
            viewHolder.textViewTitle = convertView.findViewById(R.id.text_view_title);
            viewHolder.textViewPrice = convertView.findViewById(R.id.text_view_price);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewTitle.setText(items.get(position).getTrackName());
        viewHolder.textViewGenre.setText(items.get(position).getPrimaryGenreName());
        viewHolder.textViewPrice.setText(String.valueOf(items.get(position).getTrackPrice()));

        // Using picasso for displaying image
        Picasso.with(context)
                .load(items.get(position).getArtworkUrl100())
                .placeholder(R.drawable.ic_launcher_background)
                .into(viewHolder.imageViewCover);

        return convertView;
    }

    private static class ViewHolder {
        TextView textViewTitle;
        TextView textViewGenre;
        TextView textViewPrice;
        ImageView imageViewCover;
    }


}
