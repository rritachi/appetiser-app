package local.rowell.appetiserapp;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import local.rowell.appetiserapp.Adapters.ItemAdapter;
import local.rowell.appetiserapp.Helpers.AppPreference;
import local.rowell.appetiserapp.Helpers.Singleton;
import local.rowell.appetiserapp.Models.Item;
import local.rowell.appetiserapp.Network.ApiJsonRequest;
import local.rowell.appetiserapp.Network.NetworkListener;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ListView listView;
    ArrayList<Item> itemArrayList;
    ItemAdapter itemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // display the last date visited
        String lastVisit = AppPreference.getInstance(this).getStringPreferences(AppPreference.PREFERENCE_LAST_TIME_DATE, "");
        if (!lastVisit.isEmpty()) {
           setTitle("Last Visited: " + lastVisit);
        }

        progressDialog = new ProgressDialog(this);
        listView = findViewById(R.id.list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Item item = itemArrayList.get(i);

                // get item data and save it to the preference for future use
                AppPreference.getInstance(getApplicationContext()).setDetail(AppPreference.PREFERENCE_CURRENT_VIEW, item);
                // saving the last item viewed
                AppPreference.getInstance(getApplicationContext()).setBooleanPreferences(AppPreference.PREFERENCE_HAS_VIEWED, true);

                Intent intentDetail = new Intent(MainActivity.this, DetailActivity.class);
                startActivity(intentDetail);

            }
        });

        // check if it has current viewed details
        // if true go to previously open detail
        if (AppPreference.getInstance(this).getBooleanPreferences(AppPreference.PREFERENCE_HAS_VIEWED)) {
            Intent intentDetail = new Intent(MainActivity.this, DetailActivity.class);
            startActivity(intentDetail);
        }
        else {
            fetch();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        // save the last visit time and date
        Calendar calendar = Calendar.getInstance();
        // construct date formatter
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
        // set to preference
        AppPreference.getInstance(this).setStringPreferences(AppPreference.PREFERENCE_LAST_TIME_DATE, simpleDateFormat.format(calendar.getTime()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // fetch back the list
        fetch();
    }

    /**
     * Fetch the data
     *
     */
    private void fetch() {
        progressDialog.show();
        progressDialog.setMessage(getString(R.string.fetchin));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        // fetch the apple API
        JsonObjectRequest searchRequest = new ApiJsonRequest(Request.Method.GET, getString(R.string.search), null,this, new NetworkListener() {
            @Override
            public void onRequestSuccessful(JSONObject response) {

                itemArrayList = new ArrayList<>();

                for (int x = 0; x < response.optJSONArray("results").length(); x++) {
                    try {
                        // convert the json object to Item model
                        itemArrayList.add(new Item(response.optJSONArray("results").getJSONObject(x)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                itemAdapter = new ItemAdapter(getApplicationContext(), R.layout.item_view, itemArrayList);
                listView.setAdapter(itemAdapter);

                progressDialog.dismiss();
            }

            @Override
            public void onRequestFieldError(JSONObject response) {
                progressDialog.dismiss();
            }

            @Override
            public void onRequestCommonError(JSONObject response) {
                progressDialog.dismiss();
            }

            @Override
            public void onRequestError(VolleyError error) {
                progressDialog.dismiss();
            }
        });

        // add in the queue
        Singleton.getInstance(this).addToRequestQueue(searchRequest);
    }
}
