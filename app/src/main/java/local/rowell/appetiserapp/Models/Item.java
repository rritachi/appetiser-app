package local.rowell.appetiserapp.Models;

import android.util.Log;

import org.json.JSONObject;

/**
 * Model of each item in the list
 *
 */
public class Item {

    private String kind;
    private int artistId;
    private String artistName;
    private String trackName;
    private String collectionName;
    private String artistViewUrl;
    private String tractViewUrl;
    private String artworkUrl60;
    private String artworkUrl100;
    private double trackPrice;
    private String primaryGenreName;
    private String longDescription;

    public Item(JSONObject jsonObject) {
        this.kind = jsonObject.optString("kind");
        this.artistId = jsonObject.optInt("artistId");
        this.artistName = jsonObject.optString("artistName");
        this.trackName = jsonObject.optString("trackName");
        this.collectionName = jsonObject.optString("collectionName");
        this.artistViewUrl = jsonObject.optString("artistViewUrl");
        this.tractViewUrl = jsonObject.optString("tractViewUrl");
        this.artworkUrl60 = jsonObject.optString("artworkUrl60");
        this.artworkUrl100 = jsonObject.optString("artworkUrl100");
        this.trackPrice = jsonObject.optDouble("trackPrice");
        this.primaryGenreName = jsonObject.optString("primaryGenreName");
        this.longDescription = jsonObject.optString("longDescription");
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getArtistViewUrl() {
        return artistViewUrl;
    }

    public void setArtistViewUrl(String artistViewUrl) {
        this.artistViewUrl = artistViewUrl;
    }

    public String getTractViewUrl() {
        return tractViewUrl;
    }

    public void setTractViewUrl(String tractViewUrl) {
        this.tractViewUrl = tractViewUrl;
    }

    public String getArtworkUrl60() {
        return artworkUrl60;
    }

    public void setArtworkUrl60(String artworkUrl60) {
        this.artworkUrl60 = artworkUrl60;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public void setArtworkUrl100(String artworkUrl100) {
        this.artworkUrl100 = artworkUrl100;
    }

    public double getTrackPrice() {
        return trackPrice;
    }

    public void setTrackPrice(double trackPrice) {
        this.trackPrice = trackPrice;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public void setPrimaryGenreName(String primaryGenreName) {
        this.primaryGenreName = primaryGenreName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
