package local.rowell.appetiserapp.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import local.rowell.appetiserapp.Models.Item;

/**
 * Class preference for persistent data
 *
 */
public class AppPreference {
    private static final String PREFERENCE_NAME = "share_preference";

    public static final String PREFERENCE_CURRENT_VIEW = "current_view_detail";
    public static final String PREFERENCE_HAS_VIEWED = "has_view_detail";
    public static final String PREFERENCE_LAST_TIME_DATE = "last_time_date";

    private static AppPreference singleton = null;

    private static SharedPreferences sharedPreferences;
    private static Context context;

    public synchronized static AppPreference getInstance(Context context) {
        if (singleton == null) {
            singleton = new AppPreference(context);
        }
        return (singleton);
    }

    private AppPreference(Context ctx) {
        context = ctx;
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * Set String
     *
     * @param key
     * @param value
     */
    public void setStringPreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    /**
     * Get String
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getStringPreferences(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    /**
     * Set boolean
     *
     * @param key
     * @param value
     */
    public void setBooleanPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Get boolean
     *
     * @param key
     * @return
     */
    public Boolean getBooleanPreferences(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Set a static item details
     *
     * @param key
     * @param item
     */
    public void setDetail(String key, Item item) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.putOpt("artist", item.getArtistName());
            jsonObject.putOpt("cover_image", item.getArtworkUrl100());
            jsonObject.putOpt("title", item.getTrackName());
            jsonObject.putOpt("price", String.valueOf(item.getTrackPrice()));
            jsonObject.putOpt("genre", item.getPrimaryGenreName());
            jsonObject.putOpt("description", item.getLongDescription());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        editor.putString(key, jsonObject.toString());
        editor.apply();
    }

    /**
     * Return JSONObject
     *
     * @param key
     * @return
     * @throws JSONException
     */
    public JSONObject getDetail(String key) throws JSONException {
        return new JSONObject(sharedPreferences.getString(key, "{}"));
    }

}
