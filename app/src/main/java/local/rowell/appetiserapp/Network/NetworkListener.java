package local.rowell.appetiserapp.Network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Interface class used to define the callback of network response
 *
 */
public interface NetworkListener {

    public void onRequestSuccessful(JSONObject response);
    void onRequestFieldError(JSONObject response);

    void onRequestCommonError(JSONObject response);
    public void onRequestError(VolleyError error);
}