package local.rowell.appetiserapp.Network;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import local.rowell.appetiserapp.R;

/**
 * Extend JsonObjectRequest
 *
 */
public class ApiJsonRequest extends JsonObjectRequest {

    /**
     * Constructor
     *
     * @param method
     * @param url
     * @param jsonRequest for POST data use
     * @param activity
     * @param networkListener call the interface for callback
     */
    public ApiJsonRequest(int method, String url, JSONObject jsonRequest,
                          final Activity activity, final NetworkListener networkListener) {

        super(method, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                networkListener.onRequestSuccessful(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                networkListener.onRequestError(error);

                ConnectivityManager cm =
                        (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();

                if (!isConnected) {
                    Toast.makeText(activity,
                            activity.getString(R.string.no_connection),
                            Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
