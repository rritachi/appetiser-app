package local.rowell.appetiserapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import local.rowell.appetiserapp.Helpers.AppPreference;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);

        // using preference for persistent data
        JSONObject persistentDetail = new JSONObject();
        try {
            // getting the details from the preferences
            persistentDetail = AppPreference.getInstance(this).getDetail(AppPreference.PREFERENCE_CURRENT_VIEW);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // retrieving the details
        String stringCoverUrl = persistentDetail.optString("cover_image");
        String stringPrice = persistentDetail.optString("price");
        String stringGenre = persistentDetail.optString("genre");
        String stringTitle = persistentDetail.optString("title");
        String stringArtist = persistentDetail.optString("artist");
        String stringDescription = persistentDetail.optString("description");


        ImageView coverImage = findViewById(R.id.cover_image);
        TextView textViewTitle = findViewById(R.id.text_view_title);
        TextView textViewPrice = findViewById(R.id.text_view_price);
        TextView textViewDescription = findViewById(R.id.text_view_desctiption);
        TextView textViewGenre = findViewById(R.id.text_view_genre);
        TextView textViewArtist = findViewById(R.id.text_view_artist);

        // Using picasso for displaying image
        Picasso.with(this)
                .load(stringCoverUrl)
                .placeholder(R.drawable.ic_launcher_background)
                .into(coverImage);

        // display the item
        textViewTitle.setText(stringTitle);
        textViewDescription.setText(stringDescription);
        textViewGenre.setText(stringGenre);
        textViewPrice.setText(String.valueOf(stringPrice));
        textViewArtist.setText(stringArtist);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // if back press destroy the has viewed persistent data
        AppPreference.getInstance(this).setBooleanPreferences(AppPreference.PREFERENCE_HAS_VIEWED, false);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // save the last visit time and date
        Calendar calendar = Calendar.getInstance();
        // construct date formatter
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
        // set to preference
        AppPreference.getInstance(this).setStringPreferences(AppPreference.PREFERENCE_LAST_TIME_DATE, simpleDateFormat.format(calendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // destroy the previous item viewed
                AppPreference.getInstance(this).setBooleanPreferences(AppPreference.PREFERENCE_HAS_VIEWED, false);
                finish();
                break;
        }
        return true;
    }
}
